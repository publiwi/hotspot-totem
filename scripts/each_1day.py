#!/usr/bin/env python3

#Script to run every day to synchronize contents with local database and
#store files, and post domains visited by devices to the backend

import contents, device_domains
from add_track import add_track
from print_current_track import get_current_track

if __name__ == '__main__':
    contents.sync_contents()
    add_track(get_current_track())

    if device_domains.post_user_domains():
        device_domains.delete_device_domains_locally()
