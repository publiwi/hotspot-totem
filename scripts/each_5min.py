#!/usr/bin/env python3

#Script to run every 5 minutes to synchronize confirmed devices with local
#database and iptables, and delete unconfirmed users with 10 minutes or more
#since their registration

import devices, monitor

if __name__ == '__main__':
    devices.sync_confirmeds()
    devices.delete_unconfirmeds()
    monitor.send_connections_quantity()
    monitor.send_hotspot_analisis()
    monitor.send_usb_connections_quantity()
