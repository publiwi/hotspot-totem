#!/usr/bin/env python3

import re
from scapy.all import sniff, Raw
try:
    from .client import *
except SystemError:
    from client import *

excluded_domains = ['s.amazon-adsystem.com', 'pcautivo.telcel.com', 's7.addthis.com']

def register_visit(mac_address, host):
    #Find device by MAC address
    cur.execute('SELECT id FROM devices WHERE mac_address = ?', (mac_address,))
    device = cur.fetchone()

    if device:
        try:
            #Insert if not exists
            cur.execute('INSERT INTO domains (name) VALUES (?)', (host,))
        except sqlite3.IntegrityError: pass

        cur.execute('SELECT id FROM domains WHERE name = ?', (host,))
        domain = cur.fetchone()
        #Register visit to domain
        cur.execute('INSERT INTO device_domains VALUES (?, ?)', (device[0], domain[0]))

def prn_pkt(pkt):
    mac_address = pkt['Ethernet'].src
    raw = pkt.sprintf('%Raw.load%')
    #Capture url, host and accept from raw content
    search = re.search(r'GET (.*) .*?\\r\\nHost: (?:www\.)?(.*?)\\r\\n.*Accept: (.*?)\\r\\n', raw, re.S)

    if search:
        (url, host, accept) = search.groups()

        if not re.search(r'\.(?:ico|png|bmp|gif|js|css)', url) and 'text/html' in accept and host not in excluded_domains:
            register_visit(mac_address, host)

def run():
    #Sniff all tcp traffic incomming from all hosts through port 80
    sniff(iface='eth1', filter='tcp and (dst port 80 or dst port 443)', prn=prn_pkt, store=0)

if __name__ == '__main__':
    run()
