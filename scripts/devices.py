#!/usr/bin/env python3

import subprocess, time

try:
    from .client import *
except SystemError:
    from client import *

SUDO = '/usr/bin/sudo'
ARP = '/usr/sbin/arp'
IPTABLES = '/sbin/iptables'
RMTRACK = '/usr/bin/rmtrack'
IPTABLES_SAVE = '/sbin/iptables-save'
TEE = '/usr/bin/tee'
MAC_REGEX = re.compile(r'..:..:..:..:..:..')
IP_REGEX = re.compile(r'(.{1,3}\.){3}.{1,3}')

install_opener_with_headers()

def delete_mac_address(mac_address):
    #Delete device by mac address
    cur.execute('DELETE FROM devices WHERE mac_address = ?', (mac_address,))

def delete_unconfirmeds():
    #Find all devices whose registration date more than 10 minutes
    cur.execute('SELECT mac_address FROM devices ' +
      'WHERE CAST(strftime(\'%s\', CURRENT_TIMESTAMP) as integer) - ' +
      'CAST(strftime(\'%s\', updated_at) as integer) > 600 AND confirmed = 0')
    unconfirmeds = cur.fetchall()

    for mac_address in unconfirmeds:
        try:
            delete_mac_address(mac_address[0])
            disable_mac_address(mac_address[0])
        except: pass

def confirm_devices(users_ids):
    #Confirm local devices whose user was confirmed on another hotspot
    try:
        joined_users_ids = ','.join([str(id) for id in users_ids])
        #Select all unconfirmed devices where users are in confirmed users
        cur.execute('SELECT id FROM devices ' +
            'WHERE confirmed = 0 AND user_id IN (%s)' % joined_users_ids)
        devices_to_confirm = cur.fetchall();
        joined_devices_ids = ','.join([str(d[0]) for d in devices_to_confirm])
        #Update all devices to confirmed where id is in devices to confirm
        cur.execute('UPDATE devices SET confirmed = 1 ' +
            'WHERE id IN (%s)' % joined_devices_ids)
    except: pass

def get_confirmed_users_ids():
    try:
    #Request ids from confirmed users in hotspot
        with urllib.request.urlopen('http://%s/api/get_confirmed_users_ids' % SERVER_HOST) as res:
            json_res = json.loads(res.read().decode('utf8'))
            return json_res['api']
    except urllib.error.URLError: return []

def sync_confirmeds():
    #Find all confirmed users to update local database
    confirmed_users_ids = get_confirmed_users_ids()

    if confirmed_users_ids:
        confirm_devices(confirmed_users_ids)

def register_device_locally(user_id, mac_address, confirmed):
    #Translate boolean value to SQLite3 syntax
    confirmed = 1 if confirmed else 0

    try:
        #Register new unconfirmed user locally if not exists
        cur.execute('INSERT INTO devices (user_id, mac_address, confirmed) ' +
            'VALUES (?, ?, ?)', (user_id, mac_address, confirmed))
    except sqlite3.IntegrityError: pass

def remove_connection_tracking(remote_addr):
    # The following line removes connection tracking for the PC
    # This clears any previous (incorrect) route info for the redirection
    command = '{sudo} {rmtrack} {remote_addr}'.format(
        sudo=SUDO, rmtrack=RMTRACK, remote_addr=remote_addr
    )
    subprocess.getoutput(command)
    time.sleep(1)

def save_iptables():
    #Save current iptables rules in file
    command = '{sudo} {iptables_save} | {sudo} {tee} /etc/iptables.up.rules'.format(sudo=SUDO, iptables_save=IPTABLES_SAVE, tee=TEE)
    subprocess.getoutput(command)

def disable_mac_address(mac_address):
    # Remove device from the firewall
    command = '{sudo} {iptables} -D internet -t mangle -m mac --mac-source {mac_address} -j RETURN'.format(
        sudo=SUDO, iptables=IPTABLES, mac_address=mac_address
    )
    subprocess.getoutput(command)
    save_iptables()
    remote_addr = guess_remote_addr(mac_address)

    if remote_addr:
        remove_connection_tracking(remote_addr)

def enable_mac_address(mac_address, remote_addr):
    # Add device to the firewall
    command = '{sudo} {iptables} -I internet 1 -t mangle -m mac --mac-source {mac_address} -j RETURN'.format(
        sudo=SUDO, iptables=IPTABLES, mac_address=mac_address
    )
    subprocess.getoutput(command)
    save_iptables()
    remove_connection_tracking(remote_addr)

def guess_remote_addr(mac_address):
    # Attempt to get the client's ip address
    command = '%s -n | grep %s | awk \'{print $1}\'' % (ARP, mac_address)
    arp_output = subprocess.getoutput(command)
    search = IP_REGEX.search(arp_output)

    if search:
        return search.group()

def guess_mac_address(remote_addr):
    # Attempt to get the client's mac address
    command = '{arp} -a {remote_addr}'.format(
        arp=ARP, remote_addr=remote_addr)
    arp_output = subprocess.getoutput(command)
    search = MAC_REGEX.search(arp_output)

    if search:
        return search.group()
