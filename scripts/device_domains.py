#!/usr/bin/env python3

try:
    from .client import *
except SystemError:
    from client import *

install_opener_with_headers()

def post_user_domains():
    #Function to query all daily visits to domains and post them to the backend
    cur.execute('SELECT de.user_id, do.name, COUNT(*) as visits ' +
        'FROM device_domains d ' +
        'JOIN devices de ON d.device_id = de.id ' +
        'JOIN domains do ON d.domain_id = do.id ' +
        'GROUP BY user_id, do.name')
    user_domains = cur.fetchall();

    if user_domains:
        user_domains_params = {}

        #Build params
        for i, user_domain in enumerate(user_domains):
            user_domains_params['user_domains[%d][user_id]' % i] = user_domain[0]
            user_domains_params['user_domains[%d][domain_name]' % i] = user_domain[1]
            user_domains_params['user_domains[%d][visits]' % i] = user_domain[2]

        data = urllib.parse.urlencode(user_domains_params).encode()

        try:
            #Post params
            with urllib.request.urlopen('http://%s/api/post_user_domains' % SERVER_HOST, data) as req:
                return req.getcode() == 200
        except urllib.error.URLError: return False

def delete_device_domains_locally():
    try:
        cur.execute('DELETE FROM device_visits')
    except: pass
