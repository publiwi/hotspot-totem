#!/usr/bin/env python3

import re

try:
    from .client import *
except SystemError:
    from client import *

class DSPInterface(object):
#DSP-W215 Interface to obtain watt consumption
    def __init__(self, ip):
        self.ip = ip

    def get_watts(self):
        #Request Meter Watt value to DSP device
        dsp_params = {'request': 'create_chklst'}
        data = urllib.parse.urlencode(dsp_params).encode()
        req = urllib.request.Request(url='http://%s/my_cgi.cgi' % self.ip, data=data)

        try:
            with urllib.request.urlopen(req) as res:
                lines = res.read().decode()
                #Find Meter Watt line
                search = re.search(r'Meter Watt: (.*)\n', lines, re.S)

                if search:
                    self.watts = float(search.group(1))

        except urllib.error.URLError: pass

    def get_usb_connections(self):
    #Retrieve only existing record from usb_connections table
        cur.execute('SELECT watts, quantity FROM usb_connections')
        return cur.fetchone()

    def save_watts(self):
    #Receive watts consumption then check if there is a new usb connection
        usb_connections = self.get_usb_connections()

        if usb_connections:
            watts, quantity = usb_connections

            #Check if watts has been incremented 6 (equals to 1 device) or more
            diff = self.watts - watts

            if diff > 0:
                div, mod = divmod(diff, 6)
                quantity += div

            cur.execute('UPDATE usb_connections set quantity = ?, watts = ?', (quantity, self.watts))
        else:
            cur.execute('INSERT INTO usb_connections VALUES (?, ?)', (1, self.watts))
