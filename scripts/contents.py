#!/usr/bin/env python3

import os, time
try:
    from .client import *
except SystemError:
    from client import *
from crontab import CronTab

cron = CronTab(user=True)
DOWS = ["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"]

install_opener_with_headers()

def get_true_days(days):
    #Match index of day with cron DOW value and retrieve selected days
    return [DOWS[i] for i, d in enumerate(days) if d == 1]

def delete_nonexistent_schedules(valid_contents):
    try:
        #Delete schedules that no longer exists
        schedules_ids = [str(s['id']) for ss in [c['schedules'] for c in valid_contents] for s in ss]

        #Query schedules and remove his cron jobs
        if schedules_ids:
            cur.execute('SELECT sunday, monday, tuesday, wednesday, thursday, friday, saturday, start_time, end_time ' +
                'FROM schedules WHERE id NOT IN (%s)' % ','.join(schedules_ids))
        else:
            cur.execute('SELECT sunday, monday, tuesday, wednesday, thursday, friday, saturday, start_time, end_time ' +
                'FROM schedules')

        for invalid_schedule in cur.fetchall():
            remove_job(get_true_days(invalid_schedule[:-2]), *invalid_schedule[-2:])

        cur.execute('DELETE FROM schedules WHERE id NOT IN (%s)' % ','.join(schedules_ids))
    except: pass

def delete_invalid_contents(valid_contents):
    try:
        #Delete contents with their files if already invalid
        contents_ids = [str(c['id']) for c in valid_contents]

        if contents_ids:
            cur.execute('SELECT id, file FROM contents WHERE id NOT IN (%s)' % ','.join(contents_ids))
        else:
            cur.execute('SELECT id, file FROM contents')

        for invalid_content in cur.fetchall():
            content_id = str(invalid_content[0])
            cur.execute('DELETE FROM contents WHERE id = ?', (content_id,))

            if invalid_content[1] != 'wildcard.png':
                file_path = os.path.join(PROJECT_PATH, 'static/contents', invalid_content[1])

                if os.path.exists(file_path):
                    os.remove(file_path)
    except: pass

def create_wildcard(true_days, time):
    hour, minute = time[-5:].split(':')

    #If there is no ad at time then put a wildcard
    if not any(_ for _ in cron.find_time(minute, hour, None, None, ",".join(true_days)) if _.comment == 'AD'):
        wildcard_file_path = os.path.join(PROJECT_PATH, 'static/contents', 'wildcard.png')
        initialize_job(wildcard_file_path, true_days, time, 'WILDCARD')

def initialize_job(file_path, true_days, time, comment):
    command = '%s "%s"' % (os.path.join(PROJECT_PATH, 'scripts/add_track.py'), file_path)
    job = cron.new(command)
    job.dow.on(*true_days)
    hour, minute = time[-5:].split(':')
    job.hour.on(hour)
    job.minute.on(minute)
    job.set_comment(comment)
    return job

def add_job(file_path, true_days, start_time, end_time):
    initialize_job(file_path, true_days, start_time, 'AD')
    create_wildcard(true_days, end_time)
    cron.write()

def remove_job(true_days, start_time, end_time):
    start_time_hour, start_time_minute = start_time[-5:].split(':')

    for j in cron.find_time(start_time_minute, start_time_hour, None, None, ",".join(true_days)):
        if j.comment == 'AD':
            cron.remove(j)

    end_time_hour, end_time_minute = end_time[-5:].split(':')

    #If next ad is wildcard, then re-adjust hour and minute of start time
    #else create a new job with wildcard ad
    for j in cron.find_time(end_time_minute, end_time_hour, None, None, ",".join(true_days)):
        if j.comment == 'WILDCARD':
            j.hour.on(start_time_hour)
            j.minute.on(start_time_minute)
        elif j.comment == 'AD':
            create_wildcard(true_days, start_time)

        break

    cron.write()

def update_job(file_path, old_true_days, old_start_time, old_end_time, true_days, start_time, end_time):
    old_start_time_hour, old_start_time_minute = old_start_time[-5:].split(':')

    for j in cron.find_time(old_start_time_minute, old_start_time_hour, None, None, ",".join(old_true_days)):
        if j.comment == 'AD':
            command = '%s "%s"' % (os.path.join(PROJECT_PATH, 'scripts/add_track.py'), file_path)
            j.set_command(command)
            j.dow.on(*true_days)
            hour, minute = start_time[-5:].split(':')
            j.hour.on(hour)
            j.minute.on(minute)
            create_wildcard(true_days, end_time)
            cron.write()

def process_schedule(schedule, file_basename):
    file_path = os.path.join(PROJECT_PATH, 'static/contents', file_basename)
    #DOW values of selected days to use in cron later
    true_days = [d[:3].upper() for d in schedule['true_days']]
    #Query if schedule already exists
    cur.execute('SELECT sunday, monday, tuesday, wednesday, thursday, friday, saturday, start_time, end_time ' +
        'FROM schedules WHERE id = ?', (schedule['id'],))
    old_schedule = cur.fetchone()

    if old_schedule:
        cur.execute('UPDATE schedules SET monday = ?, tuesday = ?, wednesday = ?, thursday = ?, ' +
            'friday = ?, saturday = ?, sunday = ?, start_time = ?, end_time = ? WHERE id = ?',
            (int(schedule['monday']), int(schedule['tuesday']), int(schedule['wednesday']),
            int(schedule['thursday']), int(schedule['friday']), int(schedule['saturday']),
            int(schedule['sunday']), schedule['start_time_to_s'], schedule['end_time_to_s'],
            schedule['id']))
        update_job(file_path, get_true_days(old_schedule[:-2]), old_schedule[-2], old_schedule[-1], true_days, schedule['start_time_to_s'], schedule['end_time_to_s'])
    else:
        #Insert schedule on database otherwise it's updated
        cur.execute('INSERT INTO schedules VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
            (schedule['id'], schedule['content_id'], int(schedule['monday']), int(schedule['tuesday']),
            int(schedule['wednesday']), int(schedule['thursday']), int(schedule['friday']),
            int(schedule['saturday']), int(schedule['sunday']), schedule['start_time_to_s'],
            schedule['end_time_to_s']))
        add_job(file_path, true_days, schedule['start_time_to_s'], schedule['end_time_to_s'])

def different_size(file_basename, new_size):
    #Check if file is different than new_size
    file_path = os.path.join(PROJECT_PATH, 'static/contents', file_basename)
    old_size = os.path.getsize(file_path)
    return new_size != old_size

def download_content_file(file_url, file_basename):
    retries = 0

    #Retry for 10 times each 10 seconds (equivalent to 1 minute and 40 seconds) if content can't be downloaded
    while retries < 10:
        try:
            #Download content file from server
            with urllib.request.urlopen('http://%s%s' % (SERVER_HOST, file_url)) as res:
                #If return code is 200 then there is content
                if res.getcode() == 200:
                    try:
                        file_path = os.path.join(PROJECT_PATH, 'static/contents', file_basename)

                        #Write file to disk if not exists
                        with open(file_path, 'wb') as f:
                            f.write(res.read())

                        #Download completed successfully
                        return True
                    except: break
                else: return False
        except urllib.error.URLError:
            sleep(10)
            retries += 1

    return False

def process_content(content):
    try:
        #Check if content already exists
        cur.execute('SELECT file FROM contents WHERE id = ?', str(content['id']))
        old_content = cur.fetchone()

        if old_content:
            must_update = True
            #Remove old file if its name or size was changed
            if old_content[0] != content['file_basename'] or different_size(old_content[0], content['file_size']):
                must_update = download_content_file(content['file_url'], content['file_basename'])

                if os.path.exists(old_content[0]):
                    os.remove(old_content[0])

            if must_update:
                cur.execute('UPDATE contents SET start_date = ?, end_date = ?, file = ? WHERE id = ?',
                    (content['start_date'], content['end_date'], content['file_basename'], content['id']))
        else:
            if download_content_file(content['file_url'], content['file_basename']):
                cur.execute('INSERT INTO contents VALUES (?, ?, ?, ?)',
                    (content['id'], content['start_date'], content['end_date'], content['file_basename']))

        return True
    except: return False

def remove_jobs_with(comment):
    #While needed because an strange bug
    while any(_ for _ in cron.find_comment(comment)):
        for j in cron.find_comment(comment):
            cron.remove(j)

def request_contents_list():
    retries = 0
    #Retry for 360 times each 10 seconds (equivalent to 1 hour) if main request is not completed successfully
    while retries < 360:
        try:
            #Request content list and its schedules
            with urllib.request.urlopen('http://%s/api/get_valid_content' % SERVER_HOST) as res:
                json_res = json.loads(res.read().decode('utf8'))
                return json_res['api']
        except urllib.error.URLError:
            sleep(10)
            retries += 1

    return []

def sync_contents():
    #Main function to request content list from backend and its schedules
    contents_list = request_contents_list()
    remove_jobs_with('WILDCARD')

    for content in contents_list:
        if process_content(content):
            for schedule in content['schedules']:
                process_schedule(schedule, content['file_basename'])

    #Delete all invalid and non existent schedules
    delete_invalid_contents(contents_list)
    delete_nonexistent_schedules(contents_list)

    #If there is no content then put wilcard from 00:00 to 23:59 of today
    if not contents_list:
        #Get start time and end time formatted 2000-01-01 00:00 24hrs
        start_date_formatted = time.strftime("%Y-%m-%d")
        end_date_formatted = time.strftime("%Y-%m-%d")
        #Insert wildcard content and his schedule on database
        cur.execute('INSERT INTO contents VALUES (?, ?, ?, ?)',
            (9223372036854775807, start_date_formatted, end_date_formatted, 'wildcard.png'))
        cur.execute('INSERT INTO schedules VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
            (9223372036854775807, 9223372036854775807, 1, 1, 1, 1, 1, 1, 1, '2000-01-01 00:00', '2000-01-01 23:59'))
        create_wildcard(DOWS, '2000-01-01 00:00')
        cron.write()
