#!/usr/bin/env python3

#Client to import basic modules for scripts and install a common opener for all requests

import http.cookiejar, urllib.request, urllib.parse, sqlite3, json, os, re
from time import sleep

API_AUTH_TOKEN = 'a6e4a49f5db74502bd69e4625d92b47a'
SERVER_HOST = 'vps19047.inmotionhosting.com'
PROJECT_PATH = '/home/publiwi/hotspot-totem'
con = sqlite3.connect(os.path.join(PROJECT_PATH, 'hotspot-totem.db'), isolation_level=None, check_same_thread=False)
cur = con.cursor()

#Needed class to handle error codes that actually are successfull
class BetterHTTPErrorProcessor(urllib.request.BaseHandler):
    #This method not raise an exception on status code 422 (Unprocessable entity)
    def http_error_422(self, request, response, code, msg, hdrs):
        return response

#Function to get a common opener with headers for all requests
def install_opener_with_headers():
    op = urllib.request.build_opener(BetterHTTPErrorProcessor())
    op.addheaders = [
      ('Content-Type', 'application/json'),
      ('Accept', 'application/json'),
      ('Authorization', 'Token token=%s' % API_AUTH_TOKEN)
    ]
    urllib.request.install_opener(op)
