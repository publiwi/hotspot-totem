#!/usr/bin/env python3

#Script to run every 1 minute to obtain DSP-W215 Smart Plug Watts reading
#and save value locally with quantity of new connections

from dsp_interface import DSPInterface

if __name__ == '__main__':
    dsp_interface = DSPInterface('192.168.15.46')
    dsp_interface.get_watts()
    dsp_interface.save_watts()
