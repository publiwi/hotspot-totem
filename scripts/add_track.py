#!/usr/bin/env python3

import os, dbus, pympris, sys, re
from dbus.mainloop.glib import DBusGMainLoop

def add_track(track):
    with open("/home/publiwi/.dbus-session", "r") as f:
        value = re.search(r'\'(.*)\'', f.readline()).group(1)
        os.putenv("DBUS_SESSION_BUS_ADDRESS", value)

    dbus_loop = DBusGMainLoop()
    bus = dbus.SessionBus(mainloop=dbus_loop)
    players_ids = list(pympris.available_players())

    if players_ids:
        mp = pympris.MediaPlayer(players_ids[0], bus)
        tracks = mp.track_list.Tracks

        for track_id in tracks:
            print(track_id)
            mp.track_list.RemoveTrack(track_id)

        mp.track_list.AddTrack(('file://%s' % track), '/', True)

if __name__ == '__main__':
    add_track(sys.argv[1])
