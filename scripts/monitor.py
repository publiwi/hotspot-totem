#!/usr/bin/env python3

import shlex, subprocess as sp, time

try:
    from .client import *
except SystemError:
    from client import *

try:
    from .dsp_interface import DSPInterface
except SystemError:
    from dsp_interface import DSPInterface

SUDO = '/usr/bin/sudo'
SYSTEMCTL = '/bin/systemctl'
IFCONFIG = '/sbin/ifconfig'
FREE = '/usr/bin/free'
DF = '/bin/df'
ARP_SCAN = '/usr/bin/arp-scan'
NMAP = '/usr/bin/nmap'
IP_RE = '([0-9]{1,3}\.){3}[0-9]{1,3}'
DATETIME_FORMAT = '%d/%m/%Y %I:%M %p'
DIG = '/usr/bin/dig'

install_opener_with_headers()

def send_usb_connections_quantity():
    #Retrieve quantity of USB connections then put to the backend, finally reset value to 0
    dsp_interface = DSPInterface('192.168.15.46')
    usb_connections = dsp_interface.get_usb_connections()

    if usb_connections and usb_connections[1] > 0:
        usb_connections_params = {'quantity': usb_connections[1]}

        #PUT request to the backend with the USB connections dict
        data = urllib.parse.urlencode(usb_connections_params).encode()
        req = urllib.request.Request(url='http://%s/api/put_usb_connections' % SERVER_HOST, data=data, method='PUT')

        try:
            with urllib.request.urlopen(req) as res:
                #UPDATE USB connections quantity to 0
                cur.execute('UPDATE usb_connections SET quantity = 0')
        except urllib.error.URLError: pass

def update_connections(mac_addresses):
    #Receives MAC addresses lists and checks for new, and disconnected ones
    cur.execute('SELECT mac_address FROM connections')
    stored_mac_addresses = [mac_address[0] for mac_address in cur.fetchall()]
    new_mac_addresses = list(set(mac_addresses) - set(stored_mac_addresses))
    cur.executemany('INSERT INTO connections VALUES (?)', [(mac_address,) for mac_address in new_mac_addresses])
    disconnected_mac_addresses = list(set(stored_mac_addresses) - set(mac_addresses))
    cur.execute('DELETE FROM connections WHERE mac_address IN (%s)' % ','.join('?' * len(disconnected_mac_addresses)), disconnected_mac_addresses)
    return new_mac_addresses

def get_mac_addresses():
    #Run arp-scan on eth1 interface then grep for MAC addresses
    command = '{sudo} {nmap} -sP 192.168.10.50-100 | grep -E \'^MAC Address: \' | awk \'{{print tolower($3)}}\''.format(
        sudo=SUDO, nmap=NMAP)
    output = sp.getoutput(command)
    mac_addresses = output.splitlines()
    global number_of_connections
    number_of_connections = len(mac_addresses)
    return mac_addresses

def send_connections_quantity():
    #Retrieve all MAC addresses on network then update connections with new
    #detected devices and put to the backend, finally left only active
    #connections
    mac_addresses = get_mac_addresses()
    new_mac_addresses = update_connections(mac_addresses)

    if new_mac_addresses:
        connections_params = {}

        #Build params
        for i, mac_address in enumerate(new_mac_addresses):
            connections_params['connections[%d][mac_address]' % i] = mac_address

        #PUT request to the backend with the connections dict
        data = urllib.parse.urlencode(connections_params).encode()
        req = urllib.request.Request(url='http://%s/api/put_connections' % SERVER_HOST, data=data, method='PUT')

        try:
            with urllib.request.urlopen(req) as res: pass
        except urllib.error.URLError: pass

def bool_to_int(value):
    if type(value) is bool:
        return 1 if value else 0
    else:
        return value

def get_ip_address():
    #Request public ip address from opendns.com
    command = '{dig} +short myip.opendns.com @resolver1.opendns.com'.format(
        dig=DIG)
    output = sp.getoutput(command)
    return output

def get_geolocation():
    try:
        #Request latitude and longitude from freegeoip.net webservice
        with urllib.request.urlopen('http://ip-api.com/json') as res:
            res_json = json.loads(res.read().decode())
            return {'latitude': res_json['lat'], 'longitude': res_json['lon']}
    except (ConnectionResetError, urllib.error.URLError): pass

def get_current_time_formatted():
    return time.strftime(DATETIME_FORMAT)

def get_number_of_connections():
    #Run arp-scan on eth1 interface then grep for ip addresses and counts them
    command = "{sudo} {arp_scan} -I eth1 -l 2> /dev/null | grep -E '{ip_re}' | wc -l".format(
        sudo=SUDO, arp_scan=ARP_SCAN, ip_re=IP_RE)
    output = sp.getoutput(command)
    return int(output)

def get_disk_usage():
    #Run df command then parses total and used disk, and calculate used percent
    command = '{df} | awk \'/dev\/sda/ {{printf "%.2f", $3 / $2 * 100}}\''.format(
        df=DF)
    output = sp.getoutput(command)
    return float(output)

def get_ram_usage():
    #Run free command then parses total and used ram, and calculate used percent
    command = '{free} | awk \'FNR == 2 {{printf "%.2f", $3 / $2 * 100}}\''.format(
        free=FREE)
    output = sp.getoutput(command)
    return float(output)

def get_cpu_usage():
    #Cat /proc/loadavg then prints out second column (CPU average usage during last 5 minutes)
    command = 'cat /proc/loadavg | awk \'{print $2}\''
    output = sp.getoutput(command)
    return float(output)

def get_router_up():
    #Ifconfig eth1 interface to check if network is up
    command = '{sudo} {ifconfig} eth1 | grep \'RUNNING\''.format(sudo=SUDO, ifconfig=IFCONFIG)
    (status, output) = sp.getstatusoutput(command)
    return status == 0

def get_service_up(service):
    #Receive service as argument, then check if its status is up
    command = '{sudo} {systemctl} status {service}.service'.format(
        sudo=SUDO, systemctl=SYSTEMCTL, service=service)
    p = sp.Popen(shlex.split(command), stdout=sp.PIPE)
    p.communicate()
    return p.returncode == 0

def collect_resources_usage():
    return {
        'cpu_usage': get_cpu_usage(),
        'ram_usage': get_ram_usage(),
        'disk_usage': get_disk_usage()
    }

def collect_services_status():
    return {
        'router_up': get_router_up(),
        'dnsmasq_up': get_service_up('dnsmasq'),
        'nginx_up': get_service_up('nginx'),
        'uwsgi_up': get_service_up('totem'),
        'cron_up': get_service_up('cron')
    }

def send_hotspot_analisis():
    #Collect services status, resources usage, number of connections, current
    #time formatted and post a dict to the backend
    services_status = collect_services_status()
    resources_usage = collect_resources_usage()
    current_time_formatted = get_current_time_formatted()
    geolocation = get_geolocation()
    ip_address = get_ip_address()
    analisis = {
        'last_sync': current_time_formatted,
        'number_of_connections': number_of_connections
    }
    analisis.update(services_status)
    analisis.update(resources_usage)

    if geolocation:
        analisis.update(geolocation)

    if ip_address:
        analisis.update({ 'ip_address': ip_address })

    #Convert all boolean values to integer
    analisis = { k: bool_to_int(v) for k, v in analisis.items() }

    #Dict by comprehension to obtain all hotspot params
    hotspot_params = { 'hotspot[%s]' % k: v for k, v in analisis.items() }

    #PUT request to the backend with the analisis dict
    data = urllib.parse.urlencode(hotspot_params).encode()
    req = urllib.request.Request(url='http://%s/api/put_hotspot' % SERVER_HOST, data=data, method='PUT')

    try:
        with urllib.request.urlopen(req) as res: pass
    except urllib.error.URLError: pass
