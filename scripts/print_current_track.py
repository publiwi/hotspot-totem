#!/usr/bin/env python3

import time

try:
    from .client import *
except SystemError:
    from client import *

def get_current_track():
    #Get current time formatted 00:00 24hrs (just hour)
    current_time_formatted = time.strftime("%H:%M")
    cur.execute("SELECT c.file \
      FROM schedules s \
      JOIN contents c ON s.content_id = c.id \
      WHERE TIME(s.start_time) <= :time AND TIME(s.end_time) >= :time",
      {"time": current_time_formatted}
    )
    content = cur.fetchone()
    file_basename = content[0] if content else 'wildcard.png'
    file_path = os.path.join(PROJECT_PATH, 'static/contents', file_basename)
    return file_path

if __name__ == '__main__':
    print(get_current_track())
