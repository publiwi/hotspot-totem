#/usr/bin/env python3

import os
import re
import subprocess
import logging
import json
import binascii
from logging.handlers import RotatingFileHandler
from flask import Flask, flash, request, redirect, url_for, render_template, session, abort
from user_agents import parse
from scripts.client import *
from scripts import devices

HOTSPOT_SERVER_NAME = 'portal'
HOTSPOT_DOMAIN_NAME = 'totem'
app = Flask(__name__)
app.secret_key = '$JSam^[.;as45P!2BDd/+AgR1'
#logger = logging.getLogger('Totem')
install_opener_with_headers()

#def setup_logging(logtofile, verbose):
#    logfile = os.path.join(PROJECT_PATH, 'totem.log')
#    formatter = logging.Formatter("%(asctime)s  %(name)-10s  "
#                                  "%(levelname)-8s %(message)s")
#    logger.setLevel(logging.INFO)

#    if logtofile:
#        handler = RotatingFileHandler(logfile, maxBytes=1e6, backupCount=10)
#        logger.addHandler(handler)
#        handler.setFormatter(formatter)

#    if verbose:
#        handler = logging.StreamHandler()
#        handler.setFormatter(formatter)
#        logger.addHandler(handler)
#        logger.setLevel(logging.DEBUG)

@app.route('/', methods=['GET'], defaults={'path': ''})
@app.route('/<path:path>')
def index(path):
    totem = '{hotspot_server_name}.{hotspot_domain_name}'.format(
        hotspot_server_name=HOTSPOT_SERVER_NAME,
        hotspot_domain_name=HOTSPOT_DOMAIN_NAME)

    if totem not in request.host:
        quoted_url = urllib.parse.quote(re.sub(r'^https://', 'http://', request.url))
        url = 'https://{hotspot_server_name}.{hotspot_domain_name}'.format(
            hotspot_server_name=HOTSPOT_SERVER_NAME,
            hotspot_domain_name=HOTSPOT_DOMAIN_NAME) + url_for('index') + '?goto=' + quoted_url
        return redirect(url)

    #Guess MAC Address, then check if user already has been registered
    mac_address = devices.guess_mac_address(request.remote_addr)

    if not mac_address:
        flash('''Lo lamentamos pero no podras hacer uso de Internet si no
        podemos identificar a tu equipo''', 'danger')

    return render_template('index.html')

@app.route('/login', methods=['POST'])
def login():
    mac_address = devices.guess_mac_address(request.remote_addr)

    if mac_address:
        #Send data to the backend
        login = login_user_remotely(request.form, mac_address)

        if 'errors' in login:
            return render_template('index.html', login_errors=login['errors'])
        else:
            if login['confirmed']:
                #Enable access to Internet permament
                enable_access_to_internet(login['user_id'], mac_address, confirmed=True)

                #Check for request url in goto
                url = request.form['goto']

                if url:
                    #Redirect to original url if exists
                    unquoted_url = urllib.parse.unquote(url)
                    return redirect(unquoted_url, code=303)
                else:
                    flash('''Haz iniciado sesión exitosamente, ahora puedes
                    navegar libremente''', 'success')
                    return redirect('/')
            else:
                #Enable access to Internet by 10 minutes
                enable_access_to_internet(login['user_id'], mac_address, confirmed=False)
                flash('''Encontramos que no haz confirmado tu cuenta de correo,
                tienes 10 minutos para confirmarla a través del link que fue
                enviado''', 'success')
                return redirect('/')

    else:
        #Bad request, mac address is required
        abort(400)

@app.route('/registration', methods=['POST'])
def registration():
    mac_address = devices.guess_mac_address(request.remote_addr)

    if mac_address:
        #Send data to the backend
        user = register_user_remotely(request.form, mac_address)

        if 'errors' in user:
            return render_template('index.html', user_errors=user['errors'])
        else:
            #Enable access to Internet by 10 minutes
            enable_access_to_internet(user['user']['id'], mac_address, confirmed=False)
            flash('''¡Te haz registrado con exito!, tienes 10 minutos para
            confirmar tu cuenta de correo a través del link que acabamos de
            enviar''', 'success')
            return redirect('/')

    else:
        #Bad request, mac address is required
        abort(400)

@app.route('/comment', methods=['POST'])
def comment():
    #Send data to the backend
    comment_remotely(request.form)
    flash('''¡Muchas gracias por darnos tu opinion!''', 'success')
    return redirect('/')

@app.before_request
def csrf_protect():
    #Cross site request forgery protection
    if request.method == 'POST':
        token = session.pop('_csrf_token', None)

        if not token or token != request.form['_csrf_token']:
            flash('Solicitud incorrecta, vuelve a intentar', 'danger')
            return redirect('/')

@app.template_global('csrf_token')
def generate_csrf_token():
    if '_csrf_token' not in session:
        #Generate random ascii token and store it on session
        session['_csrf_token'] = binascii.hexlify(os.urandom(16)).decode('ascii')

    return session['_csrf_token']

def enable_access_to_internet(user_id, mac_address, confirmed):
    devices.register_device_locally(user_id, mac_address, confirmed)
    devices.enable_mac_address(mac_address, request.remote_addr)

def get_device_type(user_agent):
    if user_agent.is_mobile:
        return "Mobile"
    elif user_agent.is_tablet:
        return "Tablet"
    elif user_agent.is_pc:
        return "PC"
    elif user_agent.is_bot:
        return "Bot"
    else:
        return "Other"

def get_user_agent():
    ua_string = request.headers.get('User-Agent')
    user_agent = parse(ua_string)
    return user_agent

def login_user_remotely(params, mac_address):
    #Get relevant data to post login
    user_agent = get_user_agent()
    login_params = {
      'login[username]': params['username'],
      'login[password]': params['password'],
      'device[mac_address]': mac_address,
      'device[platform]': user_agent.os.family,
      'device[device_type]': get_device_type(user_agent)
    }
    data = urllib.parse.urlencode(login_params).encode()

    #Request to register the user on the backend
    with urllib.request.urlopen('http://%s/api/post_login' % SERVER_HOST, data) as res:
        return json.loads(res.read().decode())

def register_user_remotely(params, mac_address):
    #Get relevant data to post user
    user_agent = get_user_agent()
    user_params = {
        'user[first_name]': params['first_name'],
        'user[last_name]': params['last_name'],
        'user[email]': params['email'],
        'user[password]': params['password'],
        'user[password_confirmation]': params['password_confirmation'],
        'user[gender]': params['gender'],
        'user[age]': params['age'],
        'user[devices_attributes][0][mac_address]': mac_address,
        'user[devices_attributes][0][platform]': user_agent.os.family,
        'user[devices_attributes][0][device_type]': get_device_type(user_agent)
    }
    data = urllib.parse.urlencode(user_params).encode()

    #Request to register the user on the backend
    with urllib.request.urlopen('http://%s/api/post_user' % SERVER_HOST, data) as res:
        return json.loads(res.read().decode())

def comment_remotely(params):
    #Get relevant data to post comment
    comment_params = {
        'comment[email]': params['email'],
        'comment[text]': params['text']
    }
    data = urllib.parse.urlencode(comment_params).encode()

    #Request to comment on the backend
    with urllib.request.urlopen('http://%s/api/post_comment' % SERVER_HOST, data) as res: pass

if __name__ == '__main__':
    #setup_logging(logtofile=False, verbose=True)
    app.run('0.0.0.0', debug=True)
#else:
    # Initializing in uWSGI
    #setup_logging(logtofile=False, verbose=True)
    #logger.debug('Initializing the app..')
